# -*- mode: python -*-

block_cipher = None


a = Analysis(['dt.py'],
             pathex=['D:\\Data-Science\\Programming_Assignment_2\\project_decision_tree'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='dt',
          debug=False,
          strip=False,
          upx=True,
          console=True )
